<?php
function crawl_page($url)
{
    static $seen = array();
    if (isset($seen[$url])) {
        return;
    }

    $seen[$url] = true;
    $dom = new DOMDocument('1.0');
    @$dom->loadHTMLFile($url);
    $strongs = $dom->getElementsByTagName('strong');
    $strongs = iterator_to_array($strongs);

    $vakinha['total'] = $strongs[1]->nodeValue;
    $vakinha['arrecadado'] = $strongs[2]->nodeValue;

    $spans = $dom->getElementsByTagName('span');
    $spans = iterator_to_array($spans);
    preg_match('/(\d\w*)(.\d\w*)?/', $spans[3]->nodeValue, $percent);
    // var_dump($percent[0]);die();
    $vakinha['porcentagem'] = $percent[0];
    return $vakinha;
    // echo "URL:",$url,PHP_EOL,"CONTENT:",PHP_EOL,$dom->saveHTML(),PHP_EOL,PHP_EOL;
}
$vakinha = crawl_page("https://www.vakinha.com.br/vaquinha/pousada-cultural-2015");
?>
