<?php require 'crawler-vakinha.php'
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Pousada Cultural - A comunidade irradia cultura</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	<!-- <link rel="stylesheet"type="text/css" href="/css/jquery.easy-pie-chart.css"> -->
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				<div class="header-top">
					<div class="pull-right social-icons">
						<a href="https://www.facebook.com/pousadacultural"><i class="fa fa-facebook"></i></a>
					</div>
				</div>     
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive logo-site" src="images/logo-site.png" alt="logo">
		                </a>                    
		            </div>
		            <div class="collapse navbar-collapse">
		                <ul class="nav navbar-nav navbar-right">                 
		                    <li class="scroll active"><a href="#home">Iníicio</a></li>
		                    <li class="scroll"><a href="#explore">Contagem Regressiva</a></li>                         
		                    <li class="scroll"><a href="#event">Atrações</a></li>
		                    <li class="scroll"><a href="#about">Sobre</a></li>
		                    <li><a class="scroll" href="#vakinha">CONTRIBUA COM A VAKINHA</a></li>
		                    <li class="scroll"><a href="#contact">Contato</a></li>       
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner carousel-home">
				<div class="item active">
					<img class="img-responsive" src="images/slider/bg3.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>conhece o pousada cultural? </h2>
						<h4>já estamos na segunda edição!</h4>
						<a href="https://www.facebook.com/pousadacultural" target="_blank">venha nos conhecer! <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Ajude com a Vakinha! </h2>
						<h4>É rápido e fácil</h4>
						<a href="#vakinha" >saiba mais <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/bg4.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Ajude com a Vakinha! </h2>
						<h4>É rápido e fácil</h4>
						<a href="#contact" >saiba mais <i class="fa fa-angle-right"></i></a>
					</div>
				</div>			
			</div>
		</div>    	
    </section>
	<!--/#home-->

	<section id="explore">
		<div class="container">
			<div class="row">
				<div class="watch">
					<img class="img-responsive" src="images/watch.png" alt="">
				</div>				
				<div class="col-md-4 col-md-offset-2 col-sm-5">
					<h2>O evento está chegando!</h2>
				</div>				
				<div class="col-sm-7 col-md-6">					
					<ul id="countdown">
						<li>					
							<span class="days time-font">00</span>
							<p>dias </p>
						</li>
						<li>
							<span class="hours time-font">00</span>
							<p class="">horas </p>
						</li>
						<li>
							<span class="minutes time-font">00</span>
							<p class="">minutos</p>
						</li>
						<li>
							<span class="seconds time-font">00</span>
							<p class="">segundos</p>
						</li>				
					</ul>
				</div>
			</div>
			<!-- <div class="cart">
				<a href="#"><i class="fa fa-shopping-cart"></i> <span>Purchase Tickets</span></a>
			</div> -->
		</div>
	</section><!--/#explore-->

	<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9">
					<div id="event-carousel" class="carousel slide" data-interval="false">
						<h2 class="heading">ATRAÇÕES CONFIRMADAS</h2>
						<a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="images/event/ni-brisant.jpg" alt="event-image">
											<h4>Ni Brisant</h4>
											<h5>Escritor</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="images/event/miguel-rosa.jpg" alt="event-image">
											<h4>Miguel Rosa</h4>
											<h5>Músico</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="guitar">
					<img class="img-responsive" src="images/mic.png" alt="guitar">
				</div>
			</div>			
		</div>
	</section><!--/#event-->

	<section id="about">
		<div class="guitar2">				
			<img class="img-responsive" src="images/guitar2.jpg" alt="guitar">
		</div>
		<div class="about-content">					
					<h2>O Pousada Cultural</h2>
					<p>O evento Pousada Cultural surgiu para disseminar e valorizar a cultura local, descentralizando os eixos culturais de Bauru e mostrando que a periferia tem muito a oferecer. Em setembro de 2014 foi realizada a 1ª edição do evento, contando com muita ajuda de voluntários, apoiadores e toda a energia e disposição da criançada do formigueiro. Teve música, dança, teatro, oficinas, e barracas de comida. E esse ano, estamos sonhando grande! Queremos realizar a segunda edição ainda melhor. Contaremos com atrações locais e regionais, concurso de talentos, oficinas ao ar livre, recreação, praça de alimentação, brinquedos gratuitos para as crianças e muito mais. Mas precisamos da ajuda de todos vocês, pois o evento é beneficente e é organizado pelos educadores e voluntários do Projeto Formiguinha. Queremos impactar a comunidade de forma positiva. Queremos que você faça parte dessa transformação!
					Lembrando que o POUSADA CULTURAL acontecerá dia 12 e 13 de Setembro.</p>
					<a href="https://www.facebook.com/pousadacultural" class="btn btn-primary">Veja nossa página no facebook<i class="fa fa-angle-right"></i></a>
				
		</div>
	</section><!--/#about-->
	
	<section id="vakinha">
		<div id="vakinha-feed" class="carousel slide" data-interval="false">
			<div class="twit">
				<img class="img-responsive" src="images/vakinha.png" alt="twit">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<div class='vakinha-texto'>
							Estamos fazendo uma Vakinha para ajudar na realização do evento. Para ter um evento de qualidade precisamos de <span class="vakinha-valor"><?php echo $vakinha['total']; ?></span> e já arrecadamos <span  class="vakinha-valor"><?php echo $vakinha['arrecadado']; ?></span>, <a href="https://www.vakinha.com.br/vaquinha/pousada-cultural-2015 target="_blank>nos ajude a chegar lá  <i class="fa fa-external-link"></i></a>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="vakinha-porcentagem pull-right">
							<div class="chart" data-percent="<?php echo $vakinha['porcentagem']; ?>"><span class="porcentagem-vakinha"><?php echo $vakinha['porcentagem']; ?>%</span></div>
							
						</div>
					</div>
				</div>
			</div>
		</div>		
	</section><!--/#vakinha-feed-->
	
	<section id="sponsor">
		<div id="sponsor-carousel" class="carousel slide" data-interval="false">
			<div class="container">
				<div class="row">
					<div class="col-sm-10">
						<h2>Patrocínio e Apoio</h2>			
						<a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item sponsors active">
								<div class="logo-section">
									<div><a href="#"><img class="img-responsive img-sponsor" src="http://img.bauru.sp.gov.br/website/logo/Bauru_Brasao_3.jpg" alt=""></a></div>
									<div><a href="http://www.institutosoma.org.br/"><img class="img-responsive img-sponsor" src="http://www.institutosoma.org.br/img/logo.gif" alt=""></a></div>
									<div><a href="#"><img class="img-responsive img-sponsor" src="images/sponsor/fruto-urbano.png" alt=""></a></div>
									<div><a href="#"><img class="img-responsive img-sponsor" src="images/sponsor/lotus-jr.png" alt=""></a></div>
									<div><a href="#"><img class="img-responsive img-sponsor" src="http://www.graficasubstrato.com.br/substrato/pt/img/logo_substrato.png" alt=""></a></div>
									<div><a href="#"><img class="img-responsive img-sponsor" src="http://www.superbombauru.com.br/superbombauru/pt/img/bg_logo.gif" alt=""></a></div>
								</div>
							</div>
							<!-- <div class="item">
							</div> -->
						</div>
					</div>
				</div>				
			</div>
			<div class="light">
				<img class="img-responsive" src="images/light.png" alt="">
			</div>
		</div>
	</section><!--/#sponsor-->

	<section id="contact">
		<div id="map">
			<div id="gmap-wrap">
	 			<div id="gmap"> 				
	 			</div>	 			
	    	</div>
		</div><!--/#map-->
		<div class="contact-section">
			<div class="ear-piece">
				<img class="img-responsive" src="images/ear-piece.png" alt="">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-sm-offset-4">
						<div class="contact-text">
							<h3>Contato</h3>
							<address>
								E-mail: projformiguinha@gmail.com<br>
								Telefone: (14) 3237-3106
							</address>
						</div>
						<div class="contact-address">
							<h3>Endereço</h3>
							<address>
								Rua Flávio Antônio Gonçalves, nº. 1-85<br>
								Pousada da Esperança II<br>
								Bauru/SP
							</address>
						</div>
					</div>
					<div class="col-sm-5">
						<div id="contact-section">
							<h3>Envie uma mensagem</h3>
					    	<div class="status alert alert-success" style="display: none"></div>
					    	<form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
					            <div class="form-group">
					                <input type="text" name="name" class="form-control" required="required" placeholder="Nome">
					            </div>
					            <div class="form-group">
					                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
					            </div>
					            <div class="form-group">
					                <textarea name="message" id="message" required="required" class="form-control" rows="4" placeholder="Escreva sua mensagem"></textarea>
					            </div>                        
					            <div class="form-group">
					                <button type="submit" class="btn btn-primary pull-right">Enviar</button>
					            </div>
					        </form>	       
					    </div>
					</div>
				</div>
			</div>
		</div>		
	</section>
    <!--/#contact-->

    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> Copyright  &copy;2015 Pousada Cultural                
            </div>
        </div>
    </footer>
    <!--/#footer-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
    <script type="text/javascript" src="/js/jquery.easy-pie-chart.js"></script>
  	
  	<script type="text/javascript">
	$(function() {
	    $('.chart').easyPieChart({
	    	size: 200,
	    	scaleColor: false,
		  	trackColor: '#FFE87C',
		  	barColor: '#088E4D',
		  	lineWidth: 6,
		  	lineCap: 'butt'
	    });
	});
	</script>
</body>
</html>
